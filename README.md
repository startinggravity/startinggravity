# Starting Gravity

A quick site to promote my freelance work in Drupal. Yes, this site is not a Drupal site, but don't hate on me for that. Many Drupal developers and agencies use Jekyll to power quick, mostly static sites.

This site is a heavily modified version of these open source projects:
* Freelancer Jekyll theme  [![Build Status](https://api.travis-ci.org/jeromelachaud/freelancer-theme.svg?branch=master)](https://travis-ci.org/jeromelachaud/freelancer-theme/) 
* Jekyll theme based on [Freelancer bootstrap theme ](http://startbootstrap.com/template-overviews/freelancer/)
* Jekyll [static site generator](https://jekyllrb.com/)

