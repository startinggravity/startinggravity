---
layout: inside
post-id: 6
title: East Tennessee Kidney Foundation
date: 2013-04-17
img: etkidney.jpg
img_thumb: etkidney_thumb.jpg 
project-date: April 2013
client: East Tennessee Kidney Foundation
services: Drupal 7 custom theme, content architecture, site design, logo design
description: East Tennessee Kidney Foundation didn't just need a new website as it was forming as a new organization. They also needed a visual identity. Jim designed a logo and created a clean, simple, yet strong website to help them get off the ground.   

---
