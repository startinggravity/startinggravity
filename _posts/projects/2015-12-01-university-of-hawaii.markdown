---
layout: inside
post-id: 5
title: University of Hawaii
site_url: http://www.hawaii.edu/researchcompliance/
date: 2015-12-01
img: hawaii-orc.jpg
img_thumb: hawaii-orc_thumb.jpg 
project-date: December 2015
client: University of Hawaii Office of Research Compliance
services: Drupal 7 custom theme, custom modules, content architecture, site design
description: While working for DSFederal, Jim Smith was the primary developer of a fully-responsive website for the Office of Research Compliance, located on the University of Hawaii's Manoa campus. Unfortunately, no onsite visits were included during the development time.

---
