---
layout: inside
post-id: 2
title: ORNL Small Business Programs Office
site_url: https://smallbusiness.ornl.gov
date: 2018-01-22
img: small-business-ornl.jpg
img_thumb: small-business-ornl_thumb.jpg 
project-date: January 2018
client: Oak Ridge National Laboratory
services: Drupal 8 custom theme, custom modules, content architecture, site design
description: ORNL's Small Business Programs Office promotes contract opportunities for small businesses. The staff needed to provide information in the website that was easily accessed and clearly presented. Unique reporting features were also necessary.

---
