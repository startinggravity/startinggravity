---
layout: inside
post-id: 3
title: University of Tennessee 
site_url: http://www.cci.utk.edu/
date: 2016-07-01
img: utk-cci.jpg
img_thumb: utk-cci_thumb.jpg 
project-date: July 2016
client: University of Tennessee College of Communication & Information
services: Drupal 8 custom theme, custom module
description: The University of Tennessee's Creative Communications team designed and developed a website theme for campus WordPress sites. When the university's College of Communication & Information wanted to match the style in a Drupal 8 theme, they called on Starting Gravity. 

---
