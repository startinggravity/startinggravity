---
layout: inside
post-id: 4
title: HealthIT.gov Redesign
site_url: https://beta.healthit.gov
date: 2017-02-28
img: healthit-d8.jpg
img_thumb: healthit-d8_thumb.jpg 
project-date: February 2017
client: U.S. Health & Human Services / DSFederal
services: Drupal 8 custom theme, custom module
description: DSFederal was working to meet tight deadlines in 2017 while developing a redesigned website for the U.S. Department of Health & Human Services. Starting Gravity was asked to help the effort by creating a custom module to integrate with a search API and assist in theming the site. 

---
