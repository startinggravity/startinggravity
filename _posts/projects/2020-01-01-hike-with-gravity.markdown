---
layout: inside
post-id: 1
title: Hike with Gravity
site_url: https://hikewithgravity.com
date: 2017-07-18
img: hike-with-gravity.jpg
img_thumb: hike-with-gravity_thumb.jpg 
project-date: February 2017, updated August 2020
client: Jim Smith
services: Gatsby and Drupal site with a custom theme and custom modules, including site design and logo design
description: Before Jim began a thru-hike of the Appalachian Trail in April 2017, he created a blog site in Drupal to record his daily adventures. Later, he decoupled the site to run the front-end as a Gatsby application. 

---
