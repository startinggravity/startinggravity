I"�,<p><strong>Highly skilled Drupal developer with experience in content architecture, front-end usability, back-end usability, responsive layouts, migration, and custom module and theme development.</strong></p>

<p><strong>Award-winning writer, producer and editor of video commercials and promotional announcements.</strong></p>

<p><strong>Effective manager of staff, with oversight of budgets, training and project planning.</strong></p>

<p><strong>Skilled in implementing best practices for search engine optimization, inbound marketing, and social media.</strong></p>

<p><strong>Active contributor to local and regional Drupal groups.</strong></p>

<p><strong>Successfully hiked the Triple Crown of long-distance trails (Appalachian Trail 2017, Pacific Crest Trail 2019, Continental Divide Trail 2021).</strong></p>

<hr />

<h2 id="technical-skills">TECHNICAL SKILLS</h2>

<ul>
  <li>
    <p><strong>Content Management Systems:</strong> Experience developing websites using Drupal (starting with version 4.5), GatsbyJS, Jekyll, Wordpress, Joomla, and Mambo</p>
  </li>
  <li>
    <p><strong>Coding:</strong> Detailed functional knowledge of PHP, JavaScript, jQuery, HTML5, CSS3, and Sass</p>
  </li>
  <li>
    <p><strong>Databases:</strong> MySQL, SQLite</p>
  </li>
  <li>
    <p><strong>Applications Software:</strong> PHPStorm, VisualStudio Code, Sketch, Sublime Text 2, Photoshop, Illustrator, Word, Excel, Sequel Pro</p>
  </li>
  <li>
    <p><strong>Development Tools:</strong> Git, Drush, Lando, DDEV, Gulp</p>
  </li>
  <li>
    <p><strong>Operating Systems</strong> Mac OS X, Windows 10, Linux (Ubuntu, Debian, Red Hat)</p>
  </li>
  <li>
    <p><strong>Industry Knowledge and Insight:</strong> Media, Government, Healthcare, Non-profit Advocacy, Education, Marketing, Outdoor Recreation, and Craft Beer/Homebrewing</p>
  </li>
</ul>

<hr />

<h2 id="professional-experience">PROFESSIONAL EXPERIENCE</h2>

<h3 id="freelance-drupal-developer">Freelance Drupal Developer</h3>
<p><em>Starting Gravity</em> (2011 - Present)</p>

<p>Highlights:</p>
<ul>
  <li>Provide full website development services for clients, including site building, custom module development, custom design and theming in Drupal. Also provide consulting services, professional training and instruction.</li>
</ul>

<p>Clients/Projects:</p>
<ul>
  <li>Arrow One: A variety of assignments in the development of a senior wellness site with a Drupal front-end and a Gatsby back-end</li>
  <li>ANYROAM LLC: Theme development for a migration from Drupal 7 to Drupal 8</li>
  <li>Paramount Software Solutions/Tennessee Valley Authority: Theme and custom module improvements and modifications, update core and contrib modules to latest versions</li>
  <li>2:45Tech, Knoxville: Consultation and site audit of a large Drupal 8 site for a state agency</li>
  <li>University of Tennessee College of Communication &amp; Information, Knoxville: Drupal 8 theming and custom module development</li>
  <li>Joint Institute for Computational Sciences, Oak Ridge National Laboratory and University of Tennessee: Drupal 7 custom module development, custom theme development, content architecture, site building, content migration (Drupal 6 to Drupal 7), training and consultation</li>
  <li>East Tennessee Kidney Foundation: Drupal 7 custom site and logo design, custom theming and module development, site building, training and consultation, hosting services</li>
  <li>Bluetick Brewery: Drupal 7 / Commerce custom site design, e-commerce strategy and development, custom theming, site building, training and consultation</li>
  <li>Office of Scientific and Technical Information, U.S. Department of Energy: training, custom theming and module development, site building, consultation</li>
  <li>Tennessee Clean Water Network: Drupal 6 configuration and updates, project planning for eventual migration to Drupal 7, hosting services</li>
  <li>Held Law Firm: custom theming and module development, site building, training and consultation</li>
  <li>Aspiring Web: design services, custom theming</li>
</ul>

<h3 id="thru-hiker">Thru-hiker</h3>

<p><em>Continental Divide Trail (New Mexico to Montana)</em> (April 2021 - Sept. 2021)</p>

<p>Highlights:</p>
<ul>
  <li>Successfully completed a thru-hike of more than 2800 miles from the Mexican border to the Canadian border. This hike completed long-distance hiking’s Triple Crown.</li>
</ul>

<h3 id="thru-hiker-1">Thru-hiker</h3>

<p><em>Pacific Crest Trail (California to Washington)</em> (March 2019 - Oct. 2019)</p>

<p>Highlights:</p>
<ul>
  <li>Successfully completed a thru-hike of 2650 miles from the Mexican border to the Canadian border.</li>
</ul>

<h3 id="drupal-developer">Drupal Developer</h3>

<p><em>International Information Associates (IIA)</em> (Nov. 2017 - Feb. 2019)</p>

<p>Highlights:</p>
<ul>
  <li>Team member providing website development services for Oak Ridge National Laboratory, including site migration, custom module and theme development in Drupal 7 and 8.</li>
</ul>

<p>Clients/Projects:</p>
<ul>
  <li>Sub-sections of ORNL.gov, Oak Ridge National Laboratory (Drupal 7 and 8)</li>
  <li>Small Business Programs Office, Oak Ridge National Laboratory (Drupal 8)</li>
  <li>US ITER Project (Drupal 8)</li>
</ul>

<h3 id="thru-hiker-2">Thru-hiker</h3>

<p><em>Appalachian Trail (Georgia to Maine)</em> (April 2017 - Oct. 2017)</p>

<p>Highlights:</p>
<ul>
  <li>Successfully completed a thru-hike of 2,190 miles from Springer Mountain to Mt. Katahdin.</li>
</ul>

<h3 id="drupal-developer-1">Drupal Developer</h3>

<p><em>International Information Associates (IIA)</em> (Jan. 2016 - Feb. 2017)</p>

<p>Highlights:</p>
<ul>
  <li>Team member providing website development services for Oak Ridge National Laboratory, including custom module and theme development in Drupal 7.</li>
</ul>

<p>Clients/Projects:</p>
<ul>
  <li>Subsections of ORNL.gov, Oak Ridge National Laboratory (Drupal 7)</li>
</ul>

<h3 id="front-end-drupal-developer">Front-end Drupal Developer</h3>
<p><em>DSFederal</em> (Nov. 2013 - Dec. 2015)</p>

<p>Highlights:</p>
<ul>
  <li>Team member and technical backup, providing website development services for clients, including custom module development, custom design and theming, and site building in Drupal 7 and 8, as well as project planning and Git management.</li>
</ul>

<p>Clients/Projects:</p>
<ul>
  <li>Office of the National Coordinator, U.S. Dept. of Health and Human Services: Drupal 7 and Drupal 8 custom theme development, custom module development, content architecture for HealthIT.gov</li>
  <li>University of Hawai’i at Mānoa, Office of Research Compliance: project planning, content architecture, custom theme designing and development, and site building in Drupal 7</li>
</ul>

<h3 id="director-online-media">Director, Online Media</h3>
<p><em>WATE-TV</em>, Knoxville, Tennessee (2001-2013)</p>

<p>Highlights:</p>
<ul>
  <li>Responsible for all online activities of a local affiliate of the ABC television network, including the station’s flagship website (WATE.com), other specialty websites, mobile apps, and social media.</li>
</ul>

<p>Special Projects created in Drupal:</p>
<ul>
  <li>Buy Local East Tennessee (directory site)</li>
  <li>Friday Night Hits (high school sports)</li>
  <li>Tennessee This Week (news discussion program)</li>
  <li>6 Around Town (community events)</li>
  <li>Parkside Auto Drive (auto dealer group)</li>
  <li>Rusty Wallace Toyota (auto dealer special promotion)</li>
  <li>Carpe Librum Booksellers (book reading social site)</li>
</ul>

<h3 id="other-professional-work-experience">Other professional work experience</h3>
<ul>
  <li>Marketing Director, WATE-TV, Knoxville, Tennessee (1999-2001)</li>
  <li>Freelance Promotion Writer/Producer, Home and Garden Television (HGTV), Knoxville, Tennessee (1998)</li>
  <li>Marketing Director, WVLT, Knoxville, Tennessee (1996-1998)</li>
  <li>Assistant Marketing Director and Promotion Manager, KWTV, Oklahoma City, Oklahoma (1989-1996)</li>
  <li>Promotion Manager, Senior Director, and Director, WSJV, Elkhart, Indiana (1978-1989)</li>
  <li>News Photographer, WSBT-TV, South Bend, Indiana (1978)</li>
</ul>

<hr />

<h2 id="education">EDUCATION</h2>

<h3 id="the-poynter-institute-st-petersburg-fla">The Poynter Institute, St. Petersburg, Fla.</h3>

<p>Leadership for Online News Managers (May 2006)</p>

<h3 id="indiana-university-south-bend">Indiana University, South Bend</h3>

<p>Post-graduate course work in marketing and business administration (1987-1989)</p>

<h3 id="indiana-university-bloomington">Indiana University, Bloomington</h3>

<p>Bachelor of Arts degree with major in Telecommunications and split minor in Business and Film Studies (1974-1978)</p>

<hr />

<h2 id="professional-achievements">PROFESSIONAL ACHIEVEMENTS</h2>

<h3 id="edward-r-murrow-awards-radio-television-news-directors-association">Edward R. Murrow Awards, Radio-Television News Directors Association</h3>
<ul>
  <li>Three-time winner, Best Web Site – Broadcast Affiliated – Region 8 (2003, 2004, 2006)</li>
</ul>

<h3 id="heartland-regional-emmy-awards-national-academy-of-television-arts--sciences">Heartland Regional Emmy Awards, National Academy of Television Arts &amp; Sciences</h3>
<ul>
  <li>Outstanding Individual Craft: Editing (1992)</li>
  <li>Nominee, Outstanding Informational/Instructional Program (1992)</li>
  <li>Nominee, Outstanding Individual Craft: Editing (1996)</li>
</ul>

<hr />

<h2 id="professional-and-community-affiliations">PROFESSIONAL AND COMMUNITY AFFILIATIONS</h2>

<h3 id="drupal">Drupal</h3>
<ul>
  <li>Drupal Association, Individual Member</li>
  <li>Drupal.org Member, member #16880</li>
  <li>Contributor in issue queues, Slack and IRC</li>
  <li>Session presenter at Drupal Camps
    <ul>
      <li>Asheville 2010, 2012, 2014 and 2015</li>
      <li>Nashville 2011 and 2012</li>
      <li>Charlotte 2012</li>
      <li>Chattanooga 2013, 2014, 2015, 2018 and 2019</li>
      <li>Atlanta 2014, 2015, 2018</li>
    </ul>
  </li>
  <li>DrupalCon attendee
    <ul>
      <li>Chicago 2011</li>
      <li>Denver 2012</li>
      <li>Portland 2013</li>
      <li>Austin 2014</li>
      <li>Los Angeles 2015</li>
      <li>New Orleans 2016</li>
      <li>Baltimore 2017</li>
      <li>Nashville 2018</li>
      <li>Global 2020</li>
    </ul>
  </li>
</ul>

<h3 id="boy-scouts-of-america">Boy Scouts of America</h3>
<ul>
  <li>Assistant Scoutmaster, Troop 224, Great Smoky Mountain Council (1998-2020)</li>
  <li>Committee Chair, Pack 78, Last Frontier Council (1994-1996)</li>
</ul>

<h3 id="east-tennessee-whitewater-club">East Tennessee Whitewater Club</h3>
<ul>
  <li>Vice-president (2003)</li>
  <li>Secretary (2002)</li>
</ul>

<h3 id="arts-council-of-greater-knoxville">Arts Council of Greater Knoxville</h3>
<ul>
  <li>Member, Board of Directors (1997-1998)</li>
</ul>

<h3 id="child-abuse-response-and-evaluation-center-oklahoma-city">Child Abuse Response and Evaluation Center, Oklahoma City</h3>
<ul>
  <li>Vice-president, Board of Directors (1996)</li>
  <li>Chairman, Endowment Committee (1995-1996)</li>
  <li>Treasurer, Board of Directors (1994)</li>
  <li>Member, Board of Directors (1993-1996)</li>
</ul>

<h3 id="promotion-and-marketing-executives-in-the-electronic-media-promax">Promotion and Marketing Executives in the Electronic Media (PROMAX)</h3>
<ul>
  <li>Judge, Gold Medallion Awards (1998)</li>
  <li>Conference Speaker, “Brand Equity” (1992)</li>
</ul>
:ET